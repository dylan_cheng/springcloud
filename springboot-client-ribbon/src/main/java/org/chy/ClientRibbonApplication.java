package org.chy;

import org.config.CustomerRibbonConfiguration;
import org.springframework.boot.Banner.Mode;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RibbonClient(name = "springboot-server", configuration = CustomerRibbonConfiguration.class) 
/*
 * springboot-server这个服务提供者,就使用了CustomerRibbonConfiguration类中的RandomRule策略。
 */
public class ClientRibbonApplication {

	@Bean
	@LoadBalanced
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(ClientRibbonApplication.class);
		application.setBannerMode(Mode.OFF);
		application.run(args);
	}
}
