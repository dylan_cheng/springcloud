package org.chy.controller;

import org.chy.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author  Chenghy
 * @date    2017年12月27日  02时18分15秒
 * @version 1.0
 */
@RestController
public class UserController {
	
	private @Autowired RestTemplate restTemplate;
	
	private @Autowired LoadBalancerClient loadBalancerClient;
	
	@GetMapping("getUserById/{id}")
	public User getUserById(@PathVariable("id") Integer id) {
		return restTemplate.getForObject("http://springboot-server/getUserById/" + id, User.class);
	}
		
	@GetMapping("getUserList")
	public Object getUserList() {
		//随机策略
		ServiceInstance choose = this.loadBalancerClient.choose("springboot-server");
		System.out.println(choose.getHost() + ":" + choose.getPort() + ":" + choose.getServiceId());
		//轮询策略
		ServiceInstance choose2 = this.loadBalancerClient.choose("springboot-server2");
		System.out.println(choose2.getHost() + ":" + choose2.getPort() + ":" + choose2.getServiceId());
		return "1";
	}
}
