package org.chy.config;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;

import org.springframework.boot.autoconfigure.web.BasicErrorController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Predicate;

import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author  Chenghy
 * @date    2017年12月27日  10时44分18秒
 * @version 1.0
 */
@Configuration
@EnableSwagger2
public class Swagger2Config {
	
	@Bean
	public Docket createRestApi() {
		Predicate<RequestHandler> predicate = new Predicate<RequestHandler>() {

			@Override
			public boolean apply(RequestHandler input) {
				Class<?> declaringClass = input.declaringClass();
				//排除
				if (declaringClass == BasicErrorController.class) { 
					return false;
				}
				//被注解的类
				if (declaringClass.isAnnotationPresent(RestController.class)) { 
					return true;
				}
				//被注解的方法
				if (input.isAnnotatedWith(ResponseBody.class)) {
					return true;
				}
				return false;
			}
			
		};
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo())
				.useDefaultResponseMessages(false)
				.select()
				.apis(predicate)
				.build();
				
	}
	
	@SuppressWarnings("deprecation")
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				// 大标题
				.title("swagger-boostrap-ui restful apis")
				.description("swagger-bootstrap-ui")
				.termsOfServiceUrl("http://127.0.0.1:8085/")
				.contact("--")
				.version("1.1")
				.build();
	}
}






















