package org.chy.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author  Chenghy
 * @date    2017年12月27日  11时14分20秒
 * @version 1.0
 */
@Api(value="第二控制层")
@RestController
public class SecondController {

	@ApiOperation(value="测试第二控制层第一个接口",notes="hello接口")
	@RequestMapping(value = "/testSecond", method = RequestMethod.GET)
	public String testSecond() {
		return "hello Spring boot";
	}
}
