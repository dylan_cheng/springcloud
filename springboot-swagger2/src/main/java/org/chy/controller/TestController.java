package org.chy.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * @author Chenghy
 * @date 2017年12月27日 10时58分01秒
 * @version 1.0
 */
@Api(value="测试控制层")
@RestController
public class TestController {

	//http://localhost:8085/swagger-ui.html 测试地址
	/**
	 * @Api：修饰整个类，描述Controller的作用
	 * @ApiOperation：描述一个类的一个方法，或者说一个接口
	 * @ApiParam：单个参数描述
	 * @ApiModel：用对象来接收参数
	 * @ApiProperty：用对象接收参数时，描述对象的一个字段
	 * @ApiResponse：HTTP响应其中1个描述
	 * @ApiResponses：HTTP响应整体描述
	 * @ApiIgnore：使用该注解忽略这个API
	 * @ApiError ：发生错误返回的信息
	 * @ApiParamImplicitL：一个请求参数
	 * @ApiParamsImplicit 多个请求参数 
	 */
	
	@ApiOperation(value="测试第一个接口",notes="hello接口")
	@RequestMapping(value = "/hello", method = RequestMethod.GET)
	public String hello() {
		return "hello Spring boot";
	}
	
	@ApiOperation(value="测试第二个接口",notes="测试方法带参数")
	@GetMapping("one/{id}")
	public String one(@PathVariable("id") String id) {
		return id;
	}
}
