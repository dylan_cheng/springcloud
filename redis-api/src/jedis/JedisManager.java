package jedis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.exceptions.JedisException;

/**
 * @author DITTO23
 * @date 2017年10月12日 11时23分45秒
 * @version 1.0
 */
public class JedisManager {

	private static Logger logger = LoggerFactory.getLogger(JedisManager.class);
	// redis线程池配置
	private JedisPoolConfig config;
	
	// redis线程池对象
	private static JedisPool pool;
	
	// redis的IP地址
	private String host;
	
	// redis的端口号
	private String port;

	// 初始化jedisPool
	public void init() {
		logger.info("start jedis connection pool");
		pool = new JedisPool(host, Integer.parseInt(port));
		logger.info("start jedis connection successfuly");
	}

	// 从连接池中获取连接
	public static Jedis getJedis() {
		if (pool != null) {
			return pool.getResource();
		} else {
			logger.warn("JedisPool is null,cat't get connection of redis");
			throw new NullPointerException("jedis is null");
		}
	}

	// 把连接池返回给连接池
	public static void returnResource(Jedis jedis) {
		if (jedis != null) {
			try {
				pool.returnResource(jedis);
			} catch (JedisException e) {
				logger.warn("catch JedisException when return resource", e);
			}
		}
	}

	// 关闭连接池
	public void shutDown() {
		logger.info("start to shutdown redis server");
		if (pool != null) {
			try {
				pool.destroy();
			} catch (JedisException e) {
				logger.error("destory RedisPool failed", e);
			}
		}
		logger.info("shutdown redis server successfully");
	}
}
