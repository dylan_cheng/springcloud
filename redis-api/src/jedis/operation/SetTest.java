package jedis.operation;

import jedis.pool.JedisPoolUtil;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * @author  DITTO23
 * @date    2017年10月12日  12时02分04秒
 * @version 1.0
 */
public class SetTest {
	
	public static void main(String[] args) { 
		
		JedisPool jedisPool = JedisPoolUtil.getJedisPoolInstance();
		
		Jedis jedis = null;
		
		try {
			jedis = jedisPool.getResource();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JedisPoolUtil.release(jedisPool, jedis);
		}
	}
}
