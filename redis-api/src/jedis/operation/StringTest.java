package jedis.operation;

import jedis.pool.JedisPoolUtil;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

/**
 * @author DITTO23
 * @date 2017年10月12日 11时45分09秒
 * @version 1.0
 */
public class StringTest {
	
	public static void main(String[] args) {
		JedisPool jedisPool = JedisPoolUtil.getJedisPoolInstance();
		Jedis jedis = null;
		try {
			jedis = jedisPool.getResource();
			System.out.println(jedis.get("name"));
			//设置
			jedis.set("name", "chenghaoyu");
			//判断
			if(jedis.exists("name")){
				System.out.println("存在");
			}
			System.out.println(jedis.get("name"));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JedisPoolUtil.release(jedisPool, jedis);
		}
	}
}
