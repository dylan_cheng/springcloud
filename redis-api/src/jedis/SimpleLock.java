package jedis;

import java.util.UUID;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.Transaction;

/**
 * 分布式锁
 * @author  chenghy
 * @date    2017年12月19日 21时04分47秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package jedis
 */
public class SimpleLock {

	Jedis jedis = new Jedis("192.168.200.0", 6379);
	private final String LOCK_NAME = "lock";
	
	// 获得锁  
	/**
	 * 重入锁 = 递归锁
	 * 非重入锁
	 */
	public String accquireLock(int timeout) {
		String uuid = UUID.randomUUID().toString();
		//重入锁
		long end = System.currentTimeMillis() + timeout;
		while(System.currentTimeMillis() < end) {
			// setnx原子性 
			// setnx和expire一起使用都不是原子性的了。
			if (jedis.setnx(LOCK_NAME,uuid).intValue() == 1) {
				//设置超时机制 ,让redis自动释放
				jedis.expire(LOCK_NAME, timeout);
				return uuid;
			}
			
			if (jedis.ttl(LOCK_NAME) == -1) {
				jedis.expire(LOCK_NAME, timeout);
			}
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
		return null;
	}
	
	//释放锁
	public boolean releaseLock(String uuid) {
		while(true) {
			jedis.watch(LOCK_NAME); //监控锁
			if (uuid.equals(jedis.get(LOCK_NAME))) {
				Transaction transaction = jedis.multi();
				transaction.del(LOCK_NAME);
				if (transaction.exec() == null) {
					continue;
				}
				return true;
			}
			jedis.unwatch(); //取消监控 
			break;
		}
		return false;
	}
	
	public static void main(String[] args) {
		SimpleLock simpleLock = new SimpleLock();
		String uuid = simpleLock.accquireLock(10000);
		if (uuid != null) {
			System.out.println("获得锁成功!");
		} else {
			System.out.println("失败!");
		}
	}
	
}




































