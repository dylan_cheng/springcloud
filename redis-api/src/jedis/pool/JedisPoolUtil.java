package jedis.pool;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Redis缓存池工具类
 * @author  DITTO23
 * @date    2017年10月12日  11时28分02秒
 * @version 1.0
 */
public class JedisPoolUtil {

	private JedisPoolUtil() {}

	// 被volatile修饰的变量不会被本地线程缓存,对该变量的读写都是直接操作共享内存.
	private static volatile JedisPool jedisPool = null;

	public static JedisPool getJedisPoolInstance() {
		if (jedisPool == null) {
			synchronized (JedisPoolUtil.class) {
				if (jedisPool == null) {

					JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();

					// 控制一个pool可分配多少个jedis实例
					jedisPoolConfig.setMaxActive(10000);
					// 控制一个pool最多有多少个状态为idle(空闲)的jedis实例
					jedisPoolConfig.setMaxIdle(32);
					// 当borrow一个jedis实例时,最大的等待时间,如果超过等待时间,则直接抛出JedisConnectionException
					jedisPoolConfig.setMaxWait(100 * 1000);
					// 获得一个jedis实例的时候是否检查连接可用性(ping());为true,则得到的jedis实例均是可用的.
					jedisPoolConfig.setTestOnBorrow(true);

					jedisPool = new JedisPool(jedisPoolConfig, "192.168.200.0",6379);
				}
			}
		}
		return jedisPool;
	}

	// 某个jedis对象在jedisPool池里被释放
	public static void release(JedisPool jedisPool, Jedis jedis) {
		if (jedis != null) {
			jedisPool.returnResourceObject(jedis);
		}
	}
}
