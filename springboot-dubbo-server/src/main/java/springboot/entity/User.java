package springboot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author  chenghy
 * @date    2017年12月24日 19时21分48秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package springboot.entity
 */
@Table(name="sys_user")
@Entity(name="User")
public class User extends BaseEntity {

	private static final long serialVersionUID = -7922458960192268963L;

	@Column(name="username")
	private String username;
	
	@Column(name = "password")
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
