package springboot.service;

import springboot.entity.User;

/**
 * @author  chenghy
 * @date    2017年12月24日 19时48分20秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package springboot.service
 */
public interface UserService {
	
	User findUserById(Integer id);
}
