package springboot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.config.annotation.Service;

import springboot.entity.User;
import springboot.repository.UserRepository;
import springboot.service.UserService;

/**
 * @author chenghy
 * @date 2017年12月24日 19时48分55秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package springboot.service.impl
 */
@Service(version = "1.0.0")
public class UserServiceImpl implements UserService {
	
	private @Autowired UserRepository userRepository;

	@Override
	public User findUserById(Integer id) {
		return userRepository.findOne(id);
	}

}
