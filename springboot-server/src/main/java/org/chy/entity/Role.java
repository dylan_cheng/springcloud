package org.chy.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * 角色类
 * @author  chenghy
 * @date    2018年01月12日 15时10分42秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.entity
 */
@Table(name="sys_role")
@Entity(name="Role")
public class Role extends BaseEntity {

	private static final long serialVersionUID = 9134376513396167770L;

	/*角色名称*/
	@Column(name="rolename")
	private  String rolename;

	public Role() {
	}

	public Role(Integer id, Integer status, String createBy, Date createTime, String updateBy, Date updateTime) {
		super(id, status, createBy, createTime, updateBy, updateTime);
	}

	public Role(String rolename) {
		this.rolename = rolename;
	}

	public String getRolename() {
		return rolename;
	}
	
	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "Role [rolename=" + rolename + ", getRolename()=" + getRolename() + ", getId()=" + getId()
				+ ", getStatus()=" + getStatus() + ", getCreateBy()=" + getCreateBy() + ", getCreateTime()="
				+ getCreateTime() + ", getUpdateBy()=" + getUpdateBy() + ", getUpdateTime()=" + getUpdateTime()
				+ ", hashCode()=" + hashCode() + ", getClass()=" + getClass() + ", toString()=" + super.toString()
				+ "]";
	} 
	
}
