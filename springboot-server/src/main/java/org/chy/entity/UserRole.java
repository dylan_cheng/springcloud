package org.chy.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * @author  chenghy
 * @date    2018年01月12日 15时26分21秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.entity
 */
@Table(name="sys_user_role")
@Entity(name="UserRole")
public class UserRole extends BaseEntity {

	private static final long serialVersionUID = 8359459560843186657L;

	@Column(name = "user_id")
	private String userId;
	
	@Column(name = "role_id")
	private String roleId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
