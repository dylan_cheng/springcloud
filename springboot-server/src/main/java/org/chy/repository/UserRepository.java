package org.chy.repository;

import org.chy.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * @author chenghy
 * @date 2017年12月24日 19时47分20秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package springboot.repository
 */
public interface UserRepository extends JpaRepository<User, Integer> {
	
	/**
	 * 根据用户名查询该用户对象
	 * @param username
	 * @return
	 * @date 2018年01月12日 15时50分58秒
	 */
	User findByUsername(String username);
}
