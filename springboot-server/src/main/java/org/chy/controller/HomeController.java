package org.chy.controller;

import org.chy.util.Msg;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author  chenghy
 * @date    2018年01月12日 16时24分37秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.controller
 */
@Controller
public class HomeController {

	@GetMapping("login")
	public String login () {
		return "login";
	}
	
	@RequestMapping("index")
	public String index(Model model) {
		Msg msg = new Msg("标题", "内容", "额外信息，只对管理员显示");
        model.addAttribute("msg", msg);
        return "index";
	}
}
