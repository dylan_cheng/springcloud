package org.chy.controller;

import java.util.List;

//import javax.jms.Destination;

//import org.apache.activemq.command.ActiveMQQueue;
//import org.chy.config.ActiveMQProducer;
import org.chy.entity.User;
import org.chy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author  chenghy
 * @date    2017年12月27日 22时09分18秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.controller
 */
@RestController
public class UserController {

	private @Autowired UserService userService;
	
	//消息生产者
//	private @Autowired ActiveMQProducer activeMQProducer;
	
	@GetMapping("getUserById/{id}")
	public User getUserById(@PathVariable("id") Integer id) {
		//消息生产者
//		Destination destination = new ActiveMQQueue("chy");
//		for (int i = 0; i < 10; i++) {
//			activeMQProducer.sendMessage(destination, "chenghaoyu" + i);
//		}
		return userService.findUserById(id);
	}
	
	@GetMapping("getUserList")
	public List<User> getUserList(){
		return userService.selectUserList();
	}
}
