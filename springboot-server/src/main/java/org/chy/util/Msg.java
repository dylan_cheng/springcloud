package org.chy.util;

import java.io.Serializable;

/**
 * @author chenghy
 * @date 2018年01月12日 16时26分05秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.util
 */
public class Msg implements Serializable {
	
	private static final long serialVersionUID = 3924558926489262971L;
	private String title;
	private String content;
	private String extraInfo;

	public Msg(String title, String content, String extraInfo) {
		super();
		this.title = title;
		this.content = content;
		this.extraInfo = extraInfo;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getExtraInfo() {
		return extraInfo;
	}

	public void setExtraInfo(String extraInfo) {
		this.extraInfo = extraInfo;
	}
	
}
