package org.chy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.Banner.Mode;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
//@EnableEurekaClient
public class ServerApplication {

	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(ServerApplication.class);
		application.setBannerMode(Mode.OFF);
		application.run(args);
	}
	
}
