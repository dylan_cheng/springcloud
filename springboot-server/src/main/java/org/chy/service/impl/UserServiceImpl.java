package org.chy.service.impl;

import java.util.List;

import org.chy.entity.User;
import org.chy.repository.UserRepository;
import org.chy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author  chenghy
 * @date    2017年12月27日 22时07分52秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.service.impl
 */
@Service("userService")
public class UserServiceImpl implements UserService {

	private @Autowired UserRepository userRepository;
	
	@Override
	public User findUserById(Integer id) {
		return userRepository.findOne(id);
	}

	@Override
	public List<User> selectUserList() {
		return userRepository.findAll();
	}

	@Override
	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

}
