package org.chy.config.redis;

import java.lang.reflect.Method;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;/*spring缓存*/
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * RedisConfig配置类
 * @author chenghy
 * @date 2018年01月14日 16时36分16秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.config.redis
 */
@Configuration
@EnableCaching
@SuppressWarnings({ "rawtypes", "unchecked" })
public class RedisConfig extends CachingConfigurerSupport {

	/**
	 * redis生成key的策略
	 */
	@Bean
	public KeyGenerator keyGenerator() {
		return new KeyGenerator() {
			@Override
			public Object generate(Object target, Method method, Object... params) {
				StringBuilder sb = new StringBuilder();
				sb.append(target.getClass().getName());
				sb.append(method.getName());
				for (Object obj : params) {
					sb.append(obj.toString());
				}
				return sb.toString();
			}
		};
	}
	
	/**
	 * RedisTemplate 配置,此处没有使用StringRedisTemaplate
	 * @param factory
	 * @return
	 * @date 2018年01月14日 16时54分19秒
	 */
	@Bean
	public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory factory) {
		RedisTemplate<Object, Object> redisTemplate = new RedisTemplate<>();
		redisTemplate.setConnectionFactory(factory);
		// 开启事务支持
		redisTemplate.setEnableTransactionSupport(true);
		// 使用String格式化序列化缓存键
		StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
		redisTemplate.setKeySerializer(stringRedisSerializer);
		redisTemplate.setHashKeySerializer(stringRedisSerializer);
		return redisTemplate;
	}

	/**
	 * 管理缓存 
	 * 设置缓存对象的序列化方式,不设置会报错 另外对于json序列化,对象要提供默认空构造器
	 * @param redisTemplate
	 * @return
	 * @date 2018年01月14日 17时16分30秒
	 */
	@Bean
	public CacheManager cacheManager(RedisTemplate redisTemplate) {
		// 使用Jackson2JsonRedisSerializer来序列化和反序列化redis的key和value:防止乱码
		Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
		ObjectMapper om = new ObjectMapper();
		om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
		om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
		jackson2JsonRedisSerializer.setObjectMapper(om);
		redisTemplate.setValueSerializer(jackson2JsonRedisSerializer);
		redisTemplate.setHashValueSerializer(jackson2JsonRedisSerializer);
		RedisCacheManager cacheManager = new RedisCacheManager(redisTemplate);
		// 设置缓存过期时间 （秒）
		cacheManager.setDefaultExpiration(60 * 60); 
		return cacheManager;
	}
}
