package org.chy.config.redis;

/**
 * @author  chenghy
 * @date    2018年01月14日 22时21分35秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.config.redis
 */
public interface RedisService {

	/**
	 * 写入缓存
	 * @param key
	 * @param value
	 * @date 2018年01月14日 22时38分11秒
	 */
	public boolean set(final String key,Object value);
	
	/**
	 * 按指定的时间写入
	 * @param key
	 * @param value
	 * @param expireTime
	 * @return
	 * @date 2018年01月14日 22时45分14秒
	 */
	public boolean set(final String key,Object value,Long expireTime);
	
	/**
	 * 按指定的键获取值
	 * @param key
	 * @return
	 * @date 2018年01月14日 22时39分50秒
	 */
	public Object get(String key);
	
	/**
	 * 按指定的键判断是否有对应的值
	 * @param key
	 * @return
	 * @date 2018年01月14日 22时49分26秒
	 */
	public boolean exists(final String key);
	
	/**
	 * 按指定的键删除某值
	 * @param key
	 * @date 2018年01月14日 22时50分30秒
	 */
	public void remove(final String key);
	
	/**
	 * 批量删除 key
	 * @param pattern
	 * @date 2018年01月14日 22时51分58秒
	 */
	public void removePattern(final String pattern);
	
	/**
	 * 批量删除对应的value
	 * @param keys
	 * @date 2018年01月14日 22时53分25秒
	 */
	public void remove(final String... keys);
}
