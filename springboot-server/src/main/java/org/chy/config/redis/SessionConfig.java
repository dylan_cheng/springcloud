package org.chy.config.redis;

import org.springframework.context.annotation.Configuration;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * session共享方案解决:在使用spring-session-data-redis时,基于redis的session共享问题就很简单.
 * @author  chenghy
 * @date    2018年01月14日 17时23分28秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.config.redis
 */
@Configuration
@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 30) // 86400*30
public class SessionConfig {
	// maxInactiveIntervalInSeconds:设置session失效时间,使用Redis Session之后,
	// 原Spring Boot的server.session.timeout属性不在生效
	// maxInactiveIntervalInSeconds默认是1800秒过期
}
