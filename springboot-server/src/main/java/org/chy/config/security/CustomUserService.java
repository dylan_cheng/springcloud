package org.chy.config.security;

import org.chy.entity.User;
import org.chy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.StringUtils;

/**
 * 自定义UserService需实现UserDetailsService,可直接把结果返回给SpringSecurity使用。
 * @author  chenghy
 * @date    2018年01月12日 15时51分43秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.security
 */
public class CustomUserService implements UserDetailsService {
	
	private @Autowired UserService userService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userService.findByUsername(username);
		if (StringUtils.isEmpty(user)) {
			throw new UsernameNotFoundException("用户不存在!");
		}
		return user;
	}

}
