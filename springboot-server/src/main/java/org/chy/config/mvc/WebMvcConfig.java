package org.chy.config.mvc;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * SpringMVC配置类
 * @author  chenghy
 * @date    2018年01月12日 15时57分47秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.config.mvc
 */
@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {
	
	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/login").setViewName("login");
	}
	
	/**
	 * 防止返回json的时候IE变为文件下载
	 * @return
	 * @date 2018年01月14日 22时58分35秒
	 */
	@Bean
	public MappingJackson2HttpMessageConverter getMappingJackson2HttpMessageConverter() {
		MappingJackson2HttpMessageConverter mjtms = new MappingJackson2HttpMessageConverter();
		List<MediaType> supportedMediaTypes = Arrays.asList(new MediaType[] { MediaType.TEXT_PLAIN });
		mjtms.setSupportedMediaTypes(supportedMediaTypes);
		return mjtms;
	}
}
