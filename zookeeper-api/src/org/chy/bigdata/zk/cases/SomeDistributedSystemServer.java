package org.chy.bigdata.zk.cases;

import java.io.IOException;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.ZooKeeper;

/**
 * 分布式服务器(多台服务器)
 * @author chenghy
 * @date 2017年12月03日 16时20分09秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.bigdata.zk.cases
 */
public class SomeDistributedSystemServer {

	private ZooKeeper zk = null;
	
	private void getZkClient() throws IOException {
		//服务器在需求中并不需要做任何监听
		/*
		 * 在这里需要做监听的情况是:
		 * 	 	多台服务器主从复制时,需要监听多个服务器的状态.
		 */
		zk = new ZooKeeper(GlobalConstants.zkhosts, GlobalConstants.sessiontimeout,null);

	}

	/**
	 * 向zookeeper中/servers下创建子节点 
	 * @Description: TODO 
	 * @return void 
	 * @throws InterruptedException 
	 * @throws KeeperException 
	 * @throws IOException 
	 */
	private void connectZK(String serverName, String port) throws Exception {
		
		//获取与zookeeper通信的客户端连接
		getZkClient();
		
		//先创建出父节点
		if (zk.exists(GlobalConstants.parentZnodePath, false) == null) {
			zk.create(GlobalConstants.parentZnodePath, null, Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
		}
		
		// 连接zk并创建znode
		zk.create(GlobalConstants.parentZnodePath + "/", (serverName + ":" + port).getBytes(), Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL_SEQUENTIAL);
	
		System.out.println("server " + serverName + " is online .......");
	}

	/**
	 * @throws InterruptedException
	 * 服务器的具体业务处理功能
	 * @Description: TODO 
	 * @return void 
	 * @throws
	 */
	public void handle(String serverName) throws InterruptedException {
		System.out.println("server " + serverName + " is waiting for task process.....");
		Thread.sleep(Long.MAX_VALUE);
	}

	public static void main(String[] args) throws Exception {
		SomeDistributedSystemServer server = new SomeDistributedSystemServer();

		// 一启动就去zookeeper上注册服务器信息,参数1:服务器的主机名,参数2:服务器的监听端口
		server.connectZK(args[0], args[1]);

		// 进入业务逻辑处理流程
		server.handle(args[0]); 
	}
}
