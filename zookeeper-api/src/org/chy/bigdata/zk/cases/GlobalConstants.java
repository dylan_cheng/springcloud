package org.chy.bigdata.zk.cases;
/**
 * @author  chenghy
 * @date    2017年12月03日 16时30分34秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.bigdata.zk.cases
 */
public class GlobalConstants {
	
	/**
	 * 多个请求ip和端口号
	 */
	public static final String zkhosts = "192.168.200.181:2181,192.168.200.182:2181,192.168.200.183";
	
	/**
	 * zk启动超时时间
	 */
	public static final int sessiontimeout = 2000;
	
	/**
	 * zk服务器节点的根路径
	 */
	public static final String parentZnodePath = "/servers";
}
