package org.chy.bigdata.zk.cases;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.Watcher.Event.EventType;
import org.apache.zookeeper.ZooKeeper;

/**
 * 分布式客户端(多台客户端)
 * 
 * @author chenghy
 * @date 2017年12月03日 16时21分15秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.bigdata.zk.cases
 */
public class SomeDistributedSystemClient {

	private ZooKeeper zk = null;

	/**
	 * 服务器列表
	 */
	private volatile List<String> servers = null;

	/**
	 * 获取zk连接 @Description: TODO @return void @throws
	 */
	private void getZkClient() throws IOException {

		zk = new ZooKeeper(GlobalConstants.zkhosts, GlobalConstants.sessiontimeout, new Watcher() {
			
			@Override
			public void process(WatchedEvent event) {
				
				if (event.getType().equals(EventType.None)) return ;

				try {
					// 获取新的服务器列表,重写注册监听
					updateServers();

				} catch (Exception e) {
					e.printStackTrace();
				}

			}

		});

	}

	/**
	 * 从zk中获取在线服务器信息 @Description: TODO @return void @throws
	 */
	public void updateServers() throws Exception {

		// 从servers父节点下获取到所有子节点,并注册监听
		List<String> children = zk.getChildren(GlobalConstants.parentZnodePath, true);

		ArrayList<String> serverList = new ArrayList<>();

		for (String child : children) {

			byte[] data = zk.getData(GlobalConstants.parentZnodePath + "/" + child, false, null);

			serverList.add(new String(data));
		}

		// 如果客户端是一个多线程程序,而且各个线程都会竞争访问servers列表,所以,在成员中用volatitle修饰了一个servers变量,
		// 而且更新服务器信息的这个方法中,是用一个临时List变量来进行更新.
		servers = serverList;

		// 将更新之后的服务器列表信息打印在控制台上观察一下
		for (String server : serverList) {
			System.out.println(server);
		}
	}

	/**
	 * @throws InterruptedException 业务逻辑 @Description: TODO @return void @throws
	 */
	private void requestService() throws InterruptedException {
		Thread.sleep(Long.MAX_VALUE);
	}

	public static void main(String[] args) throws Exception {

		SomeDistributedSystemClient client = new SomeDistributedSystemClient();

		// 先构造一个zk的连接
		client.getZkClient();
		
		// 获取服务器列表
		client.updateServers();

		// 客户端进入业务流程,请求服务器的服务
		client.requestService();

	}
}
