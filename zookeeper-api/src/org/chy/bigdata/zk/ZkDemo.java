package org.chy.bigdata.zk;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.Watcher.Event.EventType;
import org.apache.zookeeper.ZooDefs.Ids;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;

/**
 * 对zookeeper的增删改查的操作
 * 
 * @author chenghy
 * @date 2017年12月03日 00时50分25秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.bigdata.zk
 */
public class ZkDemo {

	private ZooKeeper zk = null;

	public void init() {
		try {
			zk = new ZooKeeper("192.168.200.181:2181", 2000, new Watcher() {
				/**
				 * 监听事件发生时的回调方法 @Description: TODO @return void @throws
				 */
				@Override
				public void process(WatchedEvent event) {
					if (event.getType() == EventType.None)
						return;
					System.out.println(event.getType() + "----" + event.getPath());

					try {
						// 再次监听
						zk.getData("/chenghaoyu2", true, null);
						// 再次监听
						zk.getChildren("/chenghaoyu2", true);

					} catch (KeeperException e) {
						e.printStackTrace();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @throws InterruptedException @throws KeeperException @throws
	 * UnsupportedEncodingException 向zookeeper服务集群中注册数据 @Description:
	 * TODO @return void @throws
	 */
	public void testCreateZnode() throws UnsupportedEncodingException, KeeperException, InterruptedException {
		// CreateMode.PERSISTENT: "永久的(只要客户端不删除,则永远存在的)"
		// zk.create("/chenghaoyu2", "这是我第一次使用zk api".getBytes("utf-8"),
		// Ids.OPEN_ACL_UNSAFE,CreateMode.PERSISTENT);
		// 创建子节点
		// zk.create("/chenghaoyu2/wupengli", "伍鹏利在绵阳一个".getBytes("utf-8"),
		// Ids.OPEN_ACL_UNSAFE,CreateMode.PERSISTENT);
		// 永久且有序号的
		// sequential的顺序维护是在一个父节点的范围之内的.
		// 如果换了一个父节点,序号的递增顺序重新开始的.
		zk.create("/chenghaoyu2/chengdezheng", "这是我们的小孩啊".getBytes("utf-8"), Ids.OPEN_ACL_UNSAFE,
				CreateMode.PERSISTENT_SEQUENTIAL);
		zk.close();
	}

	/**
	 * 从zk中删除znode @Description: TODO @return void @throws
	 */
	public void testDeleteZnode() throws InterruptedException, KeeperException {

		// 参数1:要删除的节点的路径 参数2:要删除的节点的版本,-1匹配所有的版本
		zk.delete("/chenghaoyu", -1);

		// 判断是否存在
		Stat exists = zk.exists("/chenghaoyu", false); // 不监听

		System.out.println(exists);

	}

	/**
	 * 修改zk中的节点 @Description: TODO @return void @throws
	 */
	public void testUpadateZnode() throws UnsupportedEncodingException, KeeperException, InterruptedException {

		byte[] beforeData = zk.getData("/chenghaoyu2", false, null); // null“最新的”

		System.out.println(new String(beforeData, "utf-8"));

		zk.setData("/chenghaoyu2", "我是一个优秀的人不是优秀的程序员".getBytes("utf-8"), -1);

		byte[] data = zk.getData("/chenghaoyu2", false, null); // null“最新的”

		System.out.println(new String(data, "utf-8"));

	}

	/**
	 * 在zk中指定一个节点去查询其下的所有子节点 @Description: TODO @return void @throws
	 */
	public void testGetChildrenZnode() throws KeeperException, InterruptedException {

		List<String> children = zk.getChildren("/chenghaoyu2", false);

		for (String child : children) {
			System.out.println(child);
		}
	}

	/**
	 * @throws InterruptedException @throws KeeperException zk的监听机制：
	 * 1,事先定义好监听的回调函数 2,在对znode进行各种访问操作时可以注册监听
	 * 3,监听znode上发生相应事件时,客户端zk会接收到zookeeper集群的事件通知
	 * 4,客户端zk根据事件调用我们事先定义好的回调函数 @Description: TODO @return void @throws
	 */
	public void testWatch() throws KeeperException, InterruptedException {

		// 在获取znode数据时就注册了监听
		// 监听器是一次性的,只要监听了一次事件,就失效了.
		// getDate监听的事件是数据的更改,无法监听对其节点的添加子节点,删除和修改.
		byte[] data = zk.getData("/chenghaoyu2", true, null); // true就是监听.

		// 在做查询子节点操作时注册监听
		// 监听的事件就是监听节点下的子节点变化事件
		List<String> children = zk.getChildren("/chenghaoyu2", true);

		// 事件的类型总共有:
		// EventType 这个枚举中有详解.

		/**
		 * testWatch()在主线程中,zk的守护线程就不会死了.
		 */
		Thread.sleep(Long.MAX_VALUE); // 当前线程进入睡眠状态

	}
	
	/**
	 * 模拟solr云中的配置文件集中管理
	 * 将本机上的文件(springmvc.xml)存入到zk中
	 * @Description: TODO  
	 * @return void
	 * @throws
	 */
	public void testUploadConfigFile() throws IOException, KeeperException, InterruptedException{
		
		String springmvcxml = FileUtils.readFileToString(new File("e:/springmvc.xml"), "utf-8");

		//创建一个根节点
		zk.create("/conf", null, Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT); 
		
		//再创建一个子节点，并将其赋值
		zk.create("/conf/springmvc.xml", springmvcxml.getBytes("utf-8"), Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
	
		zk.close();
	}
	
	public void deleteUploadConfigFile() throws InterruptedException, KeeperException {
		
//		zk.delete("/conf/springmvc.xml",-1); 
		
		zk.delete("/conf", -1);
		
		Stat exists = zk.exists("/conf", false);
		
		System.out.println();
	}

	public static void main(String[] args) {
		ZkDemo zkDemo = new ZkDemo();
		zkDemo.init();
			// zkDemo.testCreateZnode();
			// zkDemo.testDeleteZnode();
			// zkDemo.testUpadateZnode();
			// zkDemo.testGetChildrenZnode();
			// zkDemo.testWatch();
			// zkDemo.testUploadConfigFile();
//			zkDemo.deleteUploadConfigFile();
	}
}
