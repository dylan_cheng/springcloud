package org.chy.controller;

import org.chy.entity.User;
import org.chy.util.ResultBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author  Chenghy
 * @date    2017年12月27日  02时18分15秒
 * @version 1.0
 */
@RestController
public class UserController {
	
	private @Autowired RestTemplate restTemplate;
	
	@GetMapping("getUserById/{id}")
	public ResultBean<User> getUserById(@PathVariable("id") Integer id) {
		User user = restTemplate.getForObject("http://springboot-server/getUserById/" + id, User.class);
		return new ResultBean<User>(user);
	}
	
}
