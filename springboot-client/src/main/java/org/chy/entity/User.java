package org.chy.entity;

/**
 * @author chenghy
 * @date 2017年12月24日 19时21分48秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package springboot.entity
 */
public class User extends BaseEntity {

	private static final long serialVersionUID = -7922458960192268963L;

	private String username;

	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
