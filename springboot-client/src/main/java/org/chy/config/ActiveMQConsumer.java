package org.chy.config;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * @author  chenghy
 * @date    2018年01月02日 21时31分55秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.config
 */
@Component
public class ActiveMQConsumer {

	//使用JmsListener配置消费者监听的队列,其中text是接受到的消息
	@JmsListener(destination="chy")
	public void receiveQueue(String text) {
		System.out.println("消费者接受的消息是:" + text);
	}
}
