package org.chy.util;

import java.io.Serializable;

import lombok.Data;

/**
 * @author  chenghy
 * @date    2017年12月30日 17时30分34秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.util
 */
@Data
public class ResultBean<T> implements Serializable {

	private static final long serialVersionUID = -315546452285590042L;

	private static final int SUCCESS = 200;
	
	public static final int FAIL = 500;
	
	private String msg = "success";
	
	private int code = SUCCESS;
	
	private T data;
	
	public ResultBean() {
		super();
	}
	
	public ResultBean(T data) {
		super();
		this.data = data;
	} 
	
	public ResultBean(Throwable e) {
		super();
		this.msg = e.toString();
		this.code = FAIL;
	}
	
	public String getMsg() {
		return msg;
	} 
	
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public int getCode() {
		return code;
	}
	
	public void setCode(int code) {
		this.code = code;
	}
	
	public T getData() {
		return data;
	}
	
	public void setData(T data) {
		this.data = data;
	}
	
}
