package org.chy;

import org.springframework.boot.Banner.Mode;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient		//Eureka
public class ClientApplication {

	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(ClientApplication.class);
		application.setBannerMode(Mode.OFF);
		application.run(args);
	}
	
	@Bean
	@LoadBalanced//开启负载均衡的功能
	RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
