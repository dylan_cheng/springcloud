package org.chy.monitor;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.chy.util.ResultBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @author  chenghy
 * @date    2017年12月30日 17时25分38秒
 * @Description 发上等愿,结中等缘,享下等福;向高处立,就平处坐,从宽处行.
 * @Package org.chy.monitor
 */
@Aspect
@Component
public class ControllerAop {
	private static Logger logger = LoggerFactory.getLogger(ControllerAop.class);
	
		//@Pointcut("execution(* com.szkj.controller..*(..)) && @annotation(org.springframework.web.bind.annotation.PostMapping)")
		@Pointcut("execution(* org.chy.controller..*(..))")
		public void controllerMethodPointcut() {}
		
		@Before("controllerMethodPointcut()")
		public void doBeforeAdvice(JoinPoint joinPoint) throws Throwable {
			ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
			HttpServletRequest request = attributes.getRequest();
			logger.info("---------------------------------------------------------------------------------------");
			logger.info("---------------------------- controller log info --------------------------------------");
			logger.info("url: " + request.getRequestURL().toString());
			logger.info("http-method: " + request.getMethod());
			logger.info("ip: " + request.getRemoteAddr());
			logger.info("class-method: " + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
			logger.info("args: " + Arrays.toString(joinPoint.getArgs()));
			logger.info("---------------------------- mapper log info ------------------------------------------");
		}
		
		@Around("controllerMethodPointcut()")
		public Object doAroundAdvice(ProceedingJoinPoint proceedingJoinPoint) {
			ResultBean<?> result ;
			try {
				result = (ResultBean<?>) proceedingJoinPoint.proceed();
			} catch (Throwable e) {
				result = handlerExceptioin(proceedingJoinPoint,e);
			}
			return result;
		}
		
		private ResultBean<?> handlerExceptioin(ProceedingJoinPoint pjp,Throwable e){
			ResultBean<?> resultBean = new ResultBean<>();
			//已知异常
			if (e instanceof RuntimeException) {
				resultBean.setMsg("大胸弟,运行时异常了啊...............");
				resultBean.setCode(ResultBean.FAIL);
			}else if (e instanceof NullPointerException) {
				resultBean.setMsg("大胸弟,空指针异常了啊...............");
				resultBean.setCode(ResultBean.FAIL);
			} else if (e instanceof Exception) {
				resultBean.setMsg(e.getLocalizedMessage());
				resultBean.setCode(ResultBean.FAIL);
			} else {
				//未知的异常,应格外的注意,可以发送邮件通知等.
				resultBean.setMsg(e.toString());
				resultBean.setCode(ResultBean.FAIL);
			}
			return resultBean;
		}
}
